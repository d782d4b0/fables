import {useEffect, useState} from "react";
import {Fable} from "@fables/core";
import {from, of, Subscription} from "rxjs";

type DispatchFn<A> = (action: A) => void
export function useFable<A,S>(fable: Fable<A,S>): [S | undefined, DispatchFn<A>] {
    const [stored, setStored] = useState<S | undefined>(undefined);
    const [subscription, setSubscription] = useState<Subscription | undefined>(undefined);

    useEffect(() => {
        setSubscription((prevSubscription) => {
            if(prevSubscription) {
                prevSubscription.unsubscribe();
            }
            return from(fable()).subscribe(
                ev => {setStored(() => ev)}
            );
        })
    }, [fable]);

    return [
        stored,
        (action) => {
            fable(of(action)).subscribe();
        }
    ];
}

// insignificant change 2

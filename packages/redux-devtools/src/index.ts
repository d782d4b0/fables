import {FableEnhancer, withPassthruEnhancer} from "@fables/core";
import {tap} from "rxjs/operators";
import {addDebuggingToOperation} from "@fables/core/dist/debug/addDebuggingToOperation";

interface ReduxDevtoolsOptions {
    name?: string;
    actionCreators?: Function[] | {[name: string]: Function},
    latency?: number,
    maxAge?: number,
    trace: boolean | ((arg: any) => string | undefined)
    traceLimit: number,
    // TODO: more exist
}

type ReduxDevtoolsExtension = {
    connect(options?: ReduxDevtoolsOptions): ReduxDevtoolsConnection;
    disconnect(): void;
    send(action: any, state: any, options?: ReduxDevtoolsOptions, instanceId?: string): void;
    listen(onMessage: Function, instanceId: string): void; // TODO: improve FUnction
    open(position?: "left" | "right" | "bottom" | "panel" | "remote"): void;
    notifyErrors(onError?: Function): void; // TODO: improve Function
};

interface ReduxDevtoolsConnection {
    subscribe(listener: ReduxDevtoolsListenerFn): ReduxDevtoolsUnsubscribeFn;
    unsubscribe(): void;
    send(action: any, state: any): void;
    init(state: any): void;
    error(message: string): void;
}

type ReduxDevtoolsUnsubscribeFn = () => void;

interface ReduxDevtoolsAction {
    type: string;
    state?: any;
}

type ReduxDevtoolsListenerFn = (action: ReduxDevtoolsAction) => void;

interface FablesDevToolsGlue {
    getEnhancer<I, O>(name: string): FableEnhancer<I, O, I, O>
}

interface TypedAction {
    type: string;
}

export function initFablesDevTools(extension: ReduxDevtoolsExtension): FablesDevToolsGlue {
    if(extension == undefined) {
        console.warn("initFablesDevTools was passed extension=undefined, silently disabling");
        return <FablesDevToolsGlue>{
            getEnhancer<I, O>(name: string): FableEnhancer<I, O, I, O> {
                return withPassthruEnhancer();
            }
        };
    }
    const conn = extension.connect();
    conn.init({});

    // probably better ideologically to store this as a BehaviorSubject, but whatever
    const stateMapCache: {[name: string]: any} = {};

    return <FablesDevToolsGlue>{
        getEnhancer<I extends TypedAction, O>(name: string): FableEnhancer<I, O, I, O> {
            return nextOp => addDebuggingToOperation(input$ => input$.pipe(
                tap(action => {
                    conn.send(
                        {
                            ...action,
                            type: "IN: " + name + "/" + action.type
                        },
                        stateMapCache
                    );
                }),
                nextOp,
                tap(output => {
                    stateMapCache[name] = output;
                    conn.send(
                        {
                            type: "OUT: "+name
                        },
                        stateMapCache
                    );
                })
            ));
        }
    }
}

// insignificant change 2

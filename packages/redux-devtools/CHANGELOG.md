# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.6](https://gitlab.com/D782D4B0/fables/compare/v1.2.5...v1.2.6) (2021-06-02)

**Note:** Version bump only for package @fables/redux-devtools





## [1.2.5](https://gitlab.com/D782D4B0/fables/compare/v1.2.4...v1.2.5) (2021-05-31)

**Note:** Version bump only for package @fables/redux-devtools





## [1.2.4](https://gitlab.com/D782D4B0/fables/compare/v1.2.3...v1.2.4) (2021-05-30)

**Note:** Version bump only for package @fables/redux-devtools





## [1.2.3](https://gitlab.com/D782D4B0/fables/compare/v1.2.2...v1.2.3) (2021-05-30)


### Bug Fixes

* dummy change for re-release ([31cbb13](https://gitlab.com/D782D4B0/fables/commit/31cbb13da8c34de0d2042dc090626ed34ece71fb))





## [1.2.2](https://gitlab.com/D782D4B0/fables/compare/v1.2.1...v1.2.2) (2021-05-30)


### Bug Fixes

* missing build command in gitlab-ci, and broken build for the demo ([2c65da1](https://gitlab.com/D782D4B0/fables/commit/2c65da1b85df2bf86f4c7bac1288ea13c74e0481))





## [1.2.1](https://gitlab.com/D782D4B0/fables/compare/v1.2.0...v1.2.1) (2021-05-30)

**Note:** Version bump only for package @fables/redux-devtools





# [1.2.0](https://gitlab.com/D782D4B0/fables/compare/v1.1.1...v1.2.0) (2021-05-30)

**Note:** Version bump only for package @fables/redux-devtools





## [1.1.1](https://gitlab.com/D782D4B0/fables/compare/v1.1.0...v1.1.1) (2021-05-16)

**Note:** Version bump only for package @fables/redux-devtools





# 1.1.0 (2021-05-16)



## 1.0.1 (2021-03-23)



# 1.0.0 (2021-03-23)

**Note:** Version bump only for package @fables/redux-devtools

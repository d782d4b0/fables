# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.6](https://gitlab.com/D782D4B0/fables/compare/v1.2.5...v1.2.6) (2021-06-02)


### Bug Fixes

* immediately dispatched values are not preserved (x2, still broken) ([f45053a](https://gitlab.com/D782D4B0/fables/commit/f45053ad30cf52b2e41cf385442d49493fd0feab))





## [1.2.5](https://gitlab.com/D782D4B0/fables/compare/v1.2.4...v1.2.5) (2021-05-31)


### Bug Fixes

* immediately dispatched values are not preserved ([ece9fab](https://gitlab.com/D782D4B0/fables/commit/ece9fab3a2e8ec682fa5b059b458975183f3da47))





## [1.2.4](https://gitlab.com/D782D4B0/fables/compare/v1.2.3...v1.2.4) (2021-05-30)


### Bug Fixes

* cyclic fable definitions shouldn't error during init ([3f19853](https://gitlab.com/D782D4B0/fables/commit/3f1985369fa5a93d579ca063ed119352f3ec4abc))
* unit tests accidentally broken by prior commit ([69350da](https://gitlab.com/D782D4B0/fables/commit/69350dac995db71736ccaf6577d7b9b6b475baa3))





## [1.2.3](https://gitlab.com/D782D4B0/fables/compare/v1.2.2...v1.2.3) (2021-05-30)


### Bug Fixes

* dummy change for re-release ([31cbb13](https://gitlab.com/D782D4B0/fables/commit/31cbb13da8c34de0d2042dc090626ed34ece71fb))





## [1.2.1](https://gitlab.com/D782D4B0/fables/compare/v1.2.0...v1.2.1) (2021-05-30)


### Bug Fixes

* insignificant change to force lerna publish to actually publish x2 ([08e245b](https://gitlab.com/D782D4B0/fables/commit/08e245b2fbfd623f8c52567192925dcb9cfc706e))





# [1.2.0](https://gitlab.com/D782D4B0/fables/compare/v1.1.1...v1.2.0) (2021-05-30)


### Features

* add Debuggable v1, and adding it to FableOperation ([c36e3bc](https://gitlab.com/D782D4B0/fables/commit/c36e3bc74e6b4f739192b5971d9297d941e3ae2a))
* Fables now impl Debuggable ([6a11b62](https://gitlab.com/D782D4B0/fables/commit/6a11b62d6e6c4d1f0822ae4075e8c2cef4bb2ae9))
* missing combineFables definitions for no enhancer case ([3083c15](https://gitlab.com/D782D4B0/fables/commit/3083c15490199dcd709cd70f70a9d09ce5e8b9bc))





## [1.1.1](https://gitlab.com/D782D4B0/fables/compare/v1.1.0...v1.1.1) (2021-05-16)

**Note:** Version bump only for package @fables/core





# 1.1.0 (2021-05-16)


### Bug Fixes

* add combineEnhancers.spec.ts ([bfcdb07](https://gitlab.com/D782D4B0/fables/commit/bfcdb072bc5d1a5d5c54501e13d963953727edac))
* improved test coverage, reduced branch count in some fns ([efb5b9f](https://gitlab.com/D782D4B0/fables/commit/efb5b9f903e319fd8f571039169c66466f468ba5))


### Features

* add variableCreateFable and syncReducerCreateFable ([9c01e6b](https://gitlab.com/D782D4B0/fables/commit/9c01e6b4ecb4ae81fd35bf5f700fa4a89acd0fd6))
* initial test setup and MVP test coverage ([3033fa3](https://gitlab.com/D782D4B0/fables/commit/3033fa3ad81b2d2ce641e309b4882dd9b62ce9f7))



## 1.0.1 (2021-03-23)



# 1.0.0 (2021-03-23)

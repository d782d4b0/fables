import {Fable, FableEnhancer, FableInstantiator} from "./types";
import {createFable} from "./createFable";
import {map} from "rxjs/operators";
import {withLoopbackEnhancer} from "./enhancers/withLoopbackEnhancer";
import {singletonInstantiator} from "./instantiators/singletonInstantiator";
import {withMidstreamReducers} from "./enhancers/withMidstreamReducers";

// if no enhancer is provided, then the output type is T, same as the selector
export function createSelector<A,S,T>(
    fable$: Fable<A, S>,
    selectorFn: (state: S) => T,
): () => Fable<never,T>;

// if an enhancer is provided, then T is passed into the enhancer and the final output type is EO
export function createSelector<A,S,T,EO>(
    fable$: Fable<A, S>,
    selectorFn: (state: S) => T,
): (
    enhancer?: FableEnhancer<T, EO, never, never>,
    instantiator?: FableInstantiator<never, EO>,
) => Fable<never,EO>;

export function createSelector<A,S,T,EO>(
    fable$: Fable<A, S>,
    selectorFn: (state: S) => T,
): (
    enhancer?: FableEnhancer<T, EO, never, never>,
    instantiator?: FableInstantiator<never, EO>,
) => Fable<never,EO> {
    return function internalCreateSelector(
        enhancer = (withLoopbackEnhancer() as any), // TODO: figure out the correct type definition... in this scenario T and EO are identical
        instantiate = singletonInstantiator()
    ) {
        const fullEnhancer: FableEnhancer<never, EO, never, never> = nextOp => (
            withMidstreamReducers<never, EO, T, EO>(
                (input$) => (
                    fable$().pipe(
                        map((e) => (selectorFn(e)))
                    )
                ),
                output$ => output$
            )(
                enhancer(nextOp)
            )
        );

        return createFable<never, EO>(
            fullEnhancer,
            instantiate
        )
    }
}

import {variableCreateFable} from "./patterns/variableCreateFable";
import {of} from "rxjs";
import {createSelector} from "./createSelector";

jest.useFakeTimers();

describe('createSelector', () => {
    it('should create a derived selector that emits when its parent fable emits', () => {
        const p$ = variableCreateFable<number>();
        const s$ = createSelector(p$, v => v * 2)();
        jest.runOnlyPendingTimers()
        const fn = jest.fn();
        s$().subscribe(fn);
        expect(fn).toBeCalledTimes(0);

        p$(of(1));
        jest.runAllTimers()
        expect(fn).toBeCalledWith(2);

        p$(of(7));
        jest.runAllTimers()
        expect(fn).toBeCalledWith(14);
    })
})

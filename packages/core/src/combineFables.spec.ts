import {variableCreateFable} from "./patterns/variableCreateFable";
import {combineFables} from "./combineFables";
import {of} from "rxjs";

const TEST_VAL1 = "TEST_VAL1";
const TEST_VAL2 = "TEST_VAL2";
jest.useFakeTimers();

describe('combineFables', () => {
    it('should emit when either child observable is updated', () => {
        const c1$ = variableCreateFable<string>();
        const c2$ = variableCreateFable<string>();
        const f$ = combineFables(c1$, c2$)();
        jest.runOnlyPendingTimers()
        const fn = jest.fn();
        f$().subscribe(fn);
        expect(fn).toBeCalledTimes(0);

        // update one of the children...
        c1$(of(TEST_VAL1));
        expect(c1$.getState()).toBe(TEST_VAL1);
        expect(fn).toBeCalledTimes(0); // should not emit until BOTH have a value

        // update the other child...
        c2$(of(TEST_VAL2));
        expect(c2$.getState()).toBe(TEST_VAL2);
        expect(fn).toBeCalledTimes(1);
        expect(fn.mock.calls[0][0]).toStrictEqual([TEST_VAL1, TEST_VAL2]);
    })
})

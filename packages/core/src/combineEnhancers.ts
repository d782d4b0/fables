import {DebugEnhancerData, FableEnhancer, FableOperation, FableOperationFn} from "./types";
import {withPassthruEnhancer} from "./enhancers/withPassthruEnhancer";
import {DEBUGGABLE} from "./debug/isOperationDebuggable";
import {Observable, of} from "rxjs";
import {map, mergeMap} from "rxjs/operators";

export function combineEnhancers<I,O>(): FableEnhancer<I,O,I,O>;

export function combineEnhancers<I,O,NI,NO>(
    enh1: FableEnhancer<I,O,NI,NO>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1>(
    enh1: FableEnhancer<IMI1, IMO1, NI, NO>, enh2: FableEnhancer<I,O, IMI1, IMO1>
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2>(
    enh1: FableEnhancer<IMI2,IMO2,NI,NO>, enh2: FableEnhancer<IMI1,IMO1,IMI2,IMO2>, enh3: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2, IMI3, IMO3>(
    enh1: FableEnhancer<IMI3,IMO3,NI,NO>, enh2: FableEnhancer<IMI2,IMO2,IMI3,IMO3>, enh3: FableEnhancer<IMI1,IMO1,IMI2,IMO2>,
    enh4: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2, IMI3, IMO3, IMI4, IMO4>(
    enh1: FableEnhancer<IMI4,IMO4,NI,NO>, enh2: FableEnhancer<IMI3,IMO3,IMI4,IMO4>, enh3: FableEnhancer<IMI2,IMO2,IMI3,IMO3>,
    enh4: FableEnhancer<IMI1,IMO1,IMI2,IMO2>, enh5: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2, IMI3, IMO3, IMI4, IMO4, IMI5, IMO5>(
    enh1: FableEnhancer<IMI5,IMO5,NI,NO>, enh2: FableEnhancer<IMI4,IMO4,IMI5,IMO5>, enh3: FableEnhancer<IMI3,IMO3,IMI4,IMO4>,
    enh4: FableEnhancer<IMI2,IMO2,IMI3,IMO3>, enh5: FableEnhancer<IMI1,IMO1,IMI2,IMO2>, enh6: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2, IMI3, IMO3, IMI4, IMO4, IMI5, IMO5, IMI6, IMO6>(
    enh1: FableEnhancer<IMI6,IMO6,NI,NO>, enh2: FableEnhancer<IMI5,IMO5,IMI6,IMO6>, enh3: FableEnhancer<IMI4,IMO4,IMI5,IMO5>,
    enh4: FableEnhancer<IMI3,IMO3,IMI4,IMO4>, enh5: FableEnhancer<IMI2,IMO2,IMI3,IMO3>, enh6: FableEnhancer<IMI1,IMO1,IMI2,IMO2>,
    enh7: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2, IMI3, IMO3, IMI4, IMO4, IMI5, IMO5, IMI6, IMO6, IMI7, IMO7>(
    enh1: FableEnhancer<IMI7,IMO7,NI,NO>, enh2: FableEnhancer<IMI6,IMO6,IMI7,IMO7>, enh3: FableEnhancer<IMI5,IMO5,IMI6,IMO6>,
    enh4: FableEnhancer<IMI4,IMO4,IMI5,IMO5>, enh5: FableEnhancer<IMI3,IMO3,IMI4,IMO4>, enh6: FableEnhancer<IMI2,IMO2,IMI3,IMO3>,
    enh7: FableEnhancer<IMI1,IMO1,IMI2,IMO2>, enh8: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2, IMI3, IMO3, IMI4, IMO4, IMI5, IMO5, IMI6, IMO6, IMI7, IMO7, IMI8, IMO8>(
    enh1: FableEnhancer<IMI8,IMO8,NI,NO>, enh2: FableEnhancer<IMI7,IMO7,IMI8,IMO8>, enh3: FableEnhancer<IMI6,IMO6,IMI7,IMO7>,
    enh4: FableEnhancer<IMI5,IMO5,IMI6,IMO6>, enh5: FableEnhancer<IMI4,IMO4,IMI5,IMO5>, enh6: FableEnhancer<IMI3,IMO3,IMI4,IMO4>,
    enh7: FableEnhancer<IMI2,IMO2,IMI3,IMO3>, enh8: FableEnhancer<IMI1,IMO1,IMI2,IMO2>, enh9: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2, IMI3, IMO3, IMI4, IMO4, IMI5, IMO5, IMI6, IMO6, IMI7, IMO7, IMI8, IMO8, IMI9, IMO9>(
    enh1: FableEnhancer<IMI9,IMO9,NI,NO>, enh2: FableEnhancer<IMI8,IMO8,IMI9,IMO9>, enh3: FableEnhancer<IMI7,IMO7,IMI8,IMO8>,
    enh4: FableEnhancer<IMI6,IMO6,IMI7,IMO7>, enh5: FableEnhancer<IMI5,IMO5,IMI6,IMO6>, enh6: FableEnhancer<IMI4,IMO4,IMI5,IMO5>,
    enh7: FableEnhancer<IMI3,IMO3,IMI4,IMO4>, enh8: FableEnhancer<IMI2,IMO2,IMI3,IMO3>, enh9: FableEnhancer<IMI1,IMO1,IMI2,IMO2>,
    enh10: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2, IMI3, IMO3, IMI4, IMO4, IMI5, IMO5, IMI6, IMO6, IMI7, IMO7, IMI8, IMO8, IMI9, IMO9, IMI10, IMO10>(
    enh1: FableEnhancer<IMI10,IMO10,NI,NO>, enh2: FableEnhancer<IMI9,IMO9,IMI10,IMO10>, enh3: FableEnhancer<IMI8,IMO8,IMI9,IMO9>,
    enh4: FableEnhancer<IMI7,IMO7,IMI8,IMO8>, enh5: FableEnhancer<IMI6,IMO6,IMI7,IMO7>, enh6: FableEnhancer<IMI5,IMO5,IMI6,IMO6>,
    enh7: FableEnhancer<IMI4,IMO4,IMI5,IMO5>, enh8: FableEnhancer<IMI3,IMO3,IMI4,IMO4>, enh9: FableEnhancer<IMI2,IMO2,IMI3,IMO3>,
    enh10: FableEnhancer<IMI1,IMO1,IMI2,IMO2>, enh11: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers<I,O,NI,NO, IMI1, IMO1, IMI2, IMO2, IMI3, IMO3, IMI4, IMO4, IMI5, IMO5, IMI6, IMO6, IMI7, IMO7, IMI8, IMO8, IMI9, IMO9, IMI10, IMO10, IMI11, IMO11>(
    enh1: FableEnhancer<IMI11,IMO11,NI,NO>, enh2: FableEnhancer<IMI10,IMO10,IMI11,IMO11>, enh3: FableEnhancer<IMI9,IMO9,IMI10,IMO10>,
    enh4: FableEnhancer<IMI8,IMO8,IMI9,IMO9>, enh5: FableEnhancer<IMI7,IMO7,IMI8,IMO8>, enh6: FableEnhancer<IMI6,IMO6,IMI7,IMO7>,
    enh7: FableEnhancer<IMI5,IMO5,IMI6,IMO6>, enh8: FableEnhancer<IMI4,IMO4,IMI5,IMO5>, enh9: FableEnhancer<IMI3,IMO3,IMI4,IMO4>,
    enh10: FableEnhancer<IMI2,IMO2,IMI3,IMO3>, enh11: FableEnhancer<IMI1,IMO1,IMI2,IMO2>, enh12: FableEnhancer<I, O, IMI1, IMO1>,
): FableEnhancer<I,O,NI,NO>;

export function combineEnhancers(
    ...enhancers: FableEnhancer<any,any,any,any>[]
): FableEnhancer<any,any,any,any> {
    if (enhancers.length === 0) {
        return withPassthruEnhancer();
    }

    if (enhancers.length === 1) {
        return enhancers[0];
    }

    enhancers.reverse();

    return (nextOp) => {
        let combinedOp: FableOperation<any, any> = enhancers[enhancers.length-1](nextOp); // can be safely upcast, removing `undefined`, since item is guaranteed to exist by prior length check
        let ops: FableOperation<any, any>[] = [combinedOp];
        for (let pipelineStep of enhancers.slice(0, enhancers.length-1)) {
            combinedOp = pipelineStep(combinedOp);
            ops.push(combinedOp);
        }

        // we need to Object.assign() into the returned op to add the debugger...
        // but we also need the reference to every sub-op... so if we do that, we'll make a recursion oopsie
        // so, we need this new intermediate function specifically to assign properties to it
        const finalCombinedOp: FableOperationFn<any, any> = (input$) => combinedOp(input$);

        // this shows how many operations were passed in the PRIOR enhancer index
        const getOpLengths = (): number[] => (
            [
                0, // this is what makes the "prior" part work
                ...enhancers.map((enhancer, i) => ops[i].debugSubEnhancers().length)
            ]
        );

        const getCumulativeOpLengths = (): number[] => (
            (getOpLengths()
                .reduce(
                    (acc, opLength, i) => {
                        return [
                            ...acc,
                            opLength + (i-1 >= 0 ? acc[i-1] : 0)
                        ];
                    },
                    [] as number[]
                )
                .slice(0, -1) // the last one is for an enhancer that doesn't exist so it can be discarded
            )
        );

        const debuggableOp: FableOperation<any, any> = Object.assign(finalCombinedOp, {
            [DEBUGGABLE]: "combineEnhancers",
            debugSnapshot(): DebugEnhancerData[] {
                const cumulative = getCumulativeOpLengths();
                const f = cumulative
                    .map((opLength, i) => (
                        (ops[i].debugSnapshot()
                            .map(snap => ({...snap, index: opLength + snap.index}))
                            .reverse()
                        )
                    ));

                return f
                    .reduce((acc, v) => ([...acc, ...v]))
                    .sort((a,b) => a.index > b.index ? 1 : -1) // unnnecessary exact match as it's impossible
            },
            debugObserve(): Observable<DebugEnhancerData> {
                const cumulative = getCumulativeOpLengths();
                return of(...cumulative).pipe(
                    mergeMap((opLength, i) => (
                        ops[i].debugObserve().pipe(
                            map(snap => ({...snap, index: opLength + snap.index}))
                        )
                    ))
                );
            },
            debugSubEnhancers(): string[] {
                return ops
                    .map(op => op.debugSubEnhancers())
                    .reduce((acc, sub) => [...acc, ...sub], []);
            }
        });

        return debuggableOp;
    }


}


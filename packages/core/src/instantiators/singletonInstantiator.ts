import {BehaviorSubject, Observable} from 'rxjs';
import {Fable, FableFn, FableInstantiator} from "../types";
import {filter, map, share} from "rxjs/operators";
import {DEBUGGABLE} from "../debug/isOperationDebuggable";
import {timer} from "rxjs/internal/observable/timer";
import {mergeMap} from "rxjs/internal/operators/mergeMap";
import {ReplaySubject} from "rxjs/internal/ReplaySubject";

type ConstObjRef = {};
const CONST_OBJ_REF: ConstObjRef = {};

export const singletonInstantiator = <I,O>() => {
    const r: FableInstantiator<I,O> = (op) => {
        // this is only used for initial setup. We need to make sure these synchronously dispatched observables into the ReplaySubject
        // aren't lost if it occurs before the timer(0) in the below sequence. if you need more than 12 items then consult your physician
        // to see if Patience is right for you.
        const input$ = new ReplaySubject<Observable<I>>(12);
        const output$: BehaviorSubject<O | ConstObjRef> = new BehaviorSubject(CONST_OBJ_REF);

        const actualOutput$ = output$.pipe(
            filter(v => v !== CONST_OBJ_REF),
            map(v => v as O),
            share()
        );

        input$.pipe(
            // required to not immediately hook up all of the fables because cycles *ARE* supported, but
            // the initial variables may not all be initialized, so we can't safely call op immediately
            // so, wait for the first event loop timer check (after all variables/exports are defined)
            // and then it should be safe to call op
            mergeMap(childObs$ => childObs$),
            (mergedObs$: Observable<I>) => timer(0).pipe(
                mergeMap(() => op(mergedObs$))
            ),
        ).subscribe(
            output$ // cache output into this BehaviorSubject
        );

        const fableFn$: FableFn<I, O> = (newInput$?: Observable<I>): Observable<O> => {
            // the user does not have to specify an observable to feed in if they don't want to
            // note that this is explicitly not within a defer() because we always want to send
            // in the actions even if a .subscribe() is not called
            if(newInput$ != undefined) {
                input$.next(newInput$);
            }

            return actualOutput$;
        };

        const fable$: Fable<I,O> = Object.assign(fableFn$, {
            getState: () => {
                const v = output$.getValue();
                return v === CONST_OBJ_REF ? undefined : v as O;
            },

            [DEBUGGABLE]: "fableInstantiator",
            debugSnapshot() { return op.debugSnapshot(); },
            debugObserve() { return op.debugObserve(); },
            debugSubEnhancers() { return op.debugSubEnhancers(); }
        });

        return fable$;
    }
    return r;
}

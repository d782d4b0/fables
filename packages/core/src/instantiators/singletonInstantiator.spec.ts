import {singletonInstantiator} from "./singletonInstantiator";
import {DebugEnhancerData, Fable, FableOperation} from "../types";
import {DEBUGGABLE} from "../debug/isOperationDebuggable";
import {of} from "rxjs";
import {createFable, variableCreateFable, withReducer} from "..";

jest.useFakeTimers();

describe('singletonInstantiator', () => {
    it('should pass-thru debugging operations to the op', () => {
        const debugSnapshot: DebugEnhancerData[] = [
            {name: "asdf", index: 0, input: "asdf", output: "asdf"}
        ];

        const mockOp: FableOperation<any, any> = Object.assign(
            (() => {return of(); }),
            {
                [DEBUGGABLE]: "asdf",
                debugSnapshot() { return debugSnapshot; },
                debugObserve() { return of(...debugSnapshot); },
                debugSubEnhancers() { return ["asdf"]; }
            }
        );

        const fn = jest.fn((v) => expect(v).toStrictEqual(debugSnapshot[0]))
        const f$ = singletonInstantiator()(mockOp);

        expect(f$.debugSnapshot()).toStrictEqual(debugSnapshot);
        f$.debugObserve().subscribe(fn)
        expect(fn).toBeCalledTimes(1);
        expect(f$.debugSubEnhancers()).toStrictEqual(["asdf"]);
    });

    it('should allow for cyclic fable definitions', () => {
        const f1$: Fable<string, string> = createFable(
            withReducer<string, string>(input$ => f2$())
        )
        const f2$: Fable<string, string> = createFable(
            withReducer<string, string>(input$ => f1$())
        )
        // it shouldn't throw. that's it.
        // if there's a bug, then it would throw this:
        // ReferenceError: Cannot access 'f2$' before initialization
    });

    it('should properly ingress values from before the timer ticks', () => {
        const v$ = variableCreateFable<string>();
        v$(of("asdf"));
        jest.runAllTimers();
        expect(v$.getState()).toBe("asdf");
    })
})

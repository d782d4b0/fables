import {Fable, FableEnhancer, FableInstantiator} from "./types";
import {combineLatest, Observable} from "rxjs";
import {singletonInstantiator} from "./instantiators/singletonInstantiator";
import {createFable} from "./createFable";
import {combineEnhancers} from "./combineEnhancers";
import {withLoopbackEnhancer} from "./enhancers/withLoopbackEnhancer";
import {addDebuggingToOperation} from "./debug/addDebuggingToOperation";

function combineFables<A1,S1>(
    fable1$: Fable<A1,S1>,
): () => Fable<never,[S1]>;

function combineFables<A1,S1,A2,S2>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>,
): () => Fable<never,[S1,S2]>;

function combineFables<A1,S1,A2,S2,A3,S3>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>
): () => Fable<never,[S1,S2,S3]>;

function combineFables<A1,S1,A2,S2,A3,S3,A4,S4>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>,
): () => Fable<never,[S1,S2,S3,S4]>;

function combineFables<A1,S1,A2,S2,A3,S3,A4,S4,A5,S5>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>,
): () => Fable<never,[S1,S2,S3,S4,S5]>;

function combineFables<A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
): () => Fable<never,[S1,S2,S3,S4,S5,S6]>;

function combineFables<A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>,
): () => Fable<never,[S1,S2,S3,S4,S5,S6,S7]>;

function combineFables<A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>,
): () => Fable<never,[S1,S2,S3,S4,S5,S6,S7,S8]>;

function combineFables<A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8,A9,S9>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>, fable9$: Fable<A9,S9>,
): () => Fable<never,[S1,S2,S3,S4,S5,S6,S7,S8,S9]>;

function combineFables<A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8,A9,S9,A10,S10>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>, fable9$: Fable<A9,S9>,
    fable10$: Fable<A10,S10>,
): () => Fable<never,[S1,S2,S3,S4,S5,S6,S7,S8,S9,S10]>;

function combineFables<A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8,A9,S9,A10,S10,A11,S11>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>, fable9$: Fable<A9,S9>,
    fable10$: Fable<A10,S10>, fable11$: Fable<A11,S11>,
): () => Fable<never,[S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11]>;

function combineFables<A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8,A9,S9,A10,S10,A11,S11,A12,S12>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>, fable9$: Fable<A9,S9>,
    fable10$: Fable<A10,S10>, fable11$: Fable<A11,S11>, fable12$: Fable<A11,S11>,
): () => Fable<never,[S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12]>;

function combineFables<EO,A1,S1>(
    fable1$: Fable<A1,S1>,
): (
    enhancer?: FableEnhancer<[S1],EO,never,never>,
    instantiate?: FableInstantiator<never,EO>,
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>,
): (
    enhancer?: FableEnhancer<[S1,S2],EO,never,never>,
    instantiate?: FableInstantiator<never,EO>,
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2,A3,S3>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
) : (
    enhancer?: FableEnhancer<[S1,S2,S3],EO,never,never>,
    instantiate?: FableInstantiator<never,EO>,
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2,A3,S3,A4,S4>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>,
) : (
    enhancer?: FableEnhancer<[S1,S2,S3,S4],EO,never,never>,
    instantiate?: FableInstantiator<never,EO>,
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2,A3,S3,A4,S4,A5,S5>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>,
) : (
    enhancer?: FableEnhancer<[S1,S2,S3,S4,S5],EO,never,never>,
    instantiate?: FableInstantiator<never,EO>
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
): (
    enhancer?: FableEnhancer<[S1,S2,S3,S4,S5,S6],EO,never,never>,
    instantiate?: FableInstantiator<never,EO>
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>,
): (
    enhancer?: FableEnhancer<[S1,S2,S3,S4,S5,S6,S7],EO,never,never>,
    instantiate?: FableInstantiator<never,EO>
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>,
): (
    enhancer?: FableEnhancer<[S1,S2,S3,S4,S5,S6,S7,S8],EO,never,never>,
    instantiate?: FableInstantiator<never,EO>
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8,A9,S9>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>, fable9$: Fable<A9,S9>,
): (
    enhancer?: FableEnhancer<[S1,S2,S3,S4,S5,S6,S7,S8,S9],EO,never,never>,
    instantiate?: FableInstantiator<never,EO>,
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8,A9,S9,A10,S10>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>, fable9$: Fable<A9,S9>,
    fable10$: Fable<A10,S10>,
): (
    enhancer?: FableEnhancer<[S1,S2,S3,S4,S5,S6,S7,S8,S9,S10], EO, never, never>,
    instantiate?: FableInstantiator<never,[S1,S2,S3,S4,S5,S6,S7,S8,S9,S10]>,
) => Fable<never,EO>;

function combineFables<EO,A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8,A9,S9,A10,S10,A11,S11>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>, fable9$: Fable<A9,S9>,
    fable10$: Fable<A10,S10>, fable11$: Fable<A11,S11>,
): (
    enhancer?: FableEnhancer<[S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11], EO, never, never>,
    instantiate?: FableInstantiator<never,[S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11]>
) => Fable<never,EO>;

function combineFables<EO, A1,S1,A2,S2,A3,S3,A4,S4,A5,S5,A6,S6,A7,S7,A8,S8,A9,S9,A10,S10,A11,S11,A12,S12>(
    fable1$: Fable<A1,S1>, fable2$: Fable<A2,S2>, fable3$: Fable<A3,S3>,
    fable4$: Fable<A4,S4>, fable5$: Fable<A5,S5>, fable6$: Fable<A6,S6>,
    fable7$: Fable<A7,S7>, fable8$: Fable<A8,S8>, fable9$: Fable<A9,S9>,
    fable10$: Fable<A10,S10>, fable11$: Fable<A11,S11>, fable12$: Fable<A12,S12>,
): (
    enhancer?: FableEnhancer<[S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12], EO, never, never>,
    instantiate?: FableInstantiator<never,[S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12]>,
) => Fable<never,EO>;

function combineFables<EO>(
    ...k: any[]
): (
    enhancer_?: FableEnhancer<any, EO, never, never>,
    instantiate_?: FableInstantiator<never, any>
) => Fable<never, EO> {
    // life has no guarantees, live dangerously & buy GME
    const fables: Fable<any, any>[] = [...arguments];

    return function internalCombineFables(
        enhancer = withLoopbackEnhancer(),
        instantiate = singletonInstantiator()
    ) {
        const fable$: Observable<any[]> = combineLatest(
            ...fables
                .filter(fable => typeof fable == "function")
                .map(fable => fable())
        );

        return createFable(
            combineEnhancers(
                enhancer,
                nextOp => addDebuggingToOperation(input$ => fable$, "combineFables"),
            ),
            instantiate
        )
    }
}

export { combineFables };

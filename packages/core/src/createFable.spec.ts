import {withPassthruEnhancer} from "./enhancers/withPassthruEnhancer";
import {createFable} from "./createFable";

jest.useFakeTimers();

describe('createFable', () => {
    // TODO test positive cases

    it('should throw an exception when a non-terminating enhancer is passed in', () => {
        const e = withPassthruEnhancer<string, string>();
        // you have to cast as any because this scenario is actually IMPOSSIBLE in TS without an unsafe type cast
        // but for branch coverage, we need to test the exception coreOp within createFable
        // ... and also JS support, i guess...
        const fn = jest.fn();

        const f$ = createFable(e as any);
        const s = f$().subscribe(null, null, fn);
        expect(() => {
            jest.advanceTimersByTime(10);
        }).toThrowError();
        expect(fn).toBeCalledTimes(0);
        expect(s.closed).toBeTruthy();
    });
})

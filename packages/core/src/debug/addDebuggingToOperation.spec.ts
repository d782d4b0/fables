import {FableOperationFn, withLoopbackEnhancer, withReducer} from "..";
import {map} from "rxjs/operators";
import {addDebuggingToOperation} from "./addDebuggingToOperation";
import {Subject} from "rxjs";
import {getDebuggerName} from "./getDebuggerName";


describe('addDebuggingToEnhancer', () => {
    it('should emit the outputs from the wrapped enhancer', () => {
        const step1 = jest.fn(v => v);
        const debugObserveOutput = jest.fn();

        const enh = withReducer<number, number>(input$ => input$.pipe(
            map(value => step1(value)+1)
        ))
        const TEST_REDUCER_NAME = "reducer";

        const op = enh(null as any);

        const input$ = new Subject<number>();
        const output$ = new Subject<number>();
        op(input$).subscribe(output$);
        op.debugObserve().subscribe(debugObserveOutput);

        expect(op.debugSnapshot()).toStrictEqual([{
            index: 0,
            input: undefined,
            name: TEST_REDUCER_NAME,
            output: undefined,
        }]);
        input$.next(0);
        expect(step1).toBeCalledTimes(1);
        expect(step1).toBeCalledWith(0);
        expect(op.debugSnapshot()).toStrictEqual([{
            index: 0,
            input: 0,
            name: TEST_REDUCER_NAME,
            output: 1,
        }]);

        expect(debugObserveOutput).toBeCalledTimes(4);

        expect(debugObserveOutput).toBeCalledWith({
            index: 0,
            input: undefined,
            name: TEST_REDUCER_NAME,
        });
        expect(debugObserveOutput).toBeCalledWith({
            index: 0,
            name: TEST_REDUCER_NAME,
            output: undefined,
        });
        expect(debugObserveOutput).toBeCalledWith({
            index: 0,
            input: 0,
            name: TEST_REDUCER_NAME,
        });
        expect(debugObserveOutput).toBeCalledWith({
            index: 0,
            name: TEST_REDUCER_NAME,
            output: 1,
        });

        input$.complete();
    });

    it('should preserve the debugging name if a new one isn\'t provided', () => {
        const op = withLoopbackEnhancer<number>();
        expect(getDebuggerName(addDebuggingToOperation(op(undefined as never)))).toBe("loopback");
    })

    it('should override the debugging name if a new one is provided', () => {
        const op = withLoopbackEnhancer<number>();
        const NEW_NAME = "NEW_NAME";
        expect(getDebuggerName(addDebuggingToOperation(op(undefined as never), NEW_NAME))).toBe(NEW_NAME);
    })

    it('should create a valid debugging enhancer when no name is provided', () => {
        const rawOp: FableOperationFn<number, number> = (input$) => input$;
        expect(getDebuggerName(rawOp)).toBe("rawOp");
        const op = addDebuggingToOperation(rawOp);
        expect(getDebuggerName(op)).toBe("rawOp");

        const input$ = new Subject<number>();
        const output$ = new Subject<number>();
        op(input$).subscribe(output$);

        expect(op.debugSubEnhancers().length).toBe(1);
        expect(op.debugSnapshot()).toStrictEqual([{
            index: 0,
            input: undefined,
            name: "rawOp",
            output: undefined,
        }]);
        input$.next(50);
        expect(op.debugSnapshot()).toStrictEqual([{
            index: 0,
            input: 50,
            name: "rawOp",
            output: 50,
        }]);
    })
})

import {Debuggable} from "../types";
import {DEBUGGABLE, isDebuggable} from "./isOperationDebuggable";

export function getDebuggerName(d: Debuggable | object | ((...arg: any[]) => any)): string {
    if(isDebuggable(d)) {
        return d[DEBUGGABLE];
    }
    if('name' in d && d.name) {
        return d.name;
    }
    return "unknownDebuggable";
}

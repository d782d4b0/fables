import {getDebuggerName} from "./getDebuggerName";

describe('getEnhancerName', () => {
    it('should use the function .name for fn', () => {
        expect(getDebuggerName(function dummy1(){} as any)).toBe("dummy1");
    })
    it('should use the variable .name for anonymous fn', () => {
        const dummy2 = function(){} as any;
        expect(getDebuggerName(dummy2)).toBe("dummy2");
    })
    it('should prefer the function .name preferably, even when the prototype name exists', () => {
        const t = function dummy3(){} as any;
        expect(getDebuggerName(t)).toBe("dummy3");
    })
    it('should return unknownEnhancer for unnamed enhancers', () => {
        expect(getDebuggerName((() => {}) as any)).toBe("unknownDebuggable");
    })
})

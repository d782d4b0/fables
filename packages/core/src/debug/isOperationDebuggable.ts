import {Debuggable, FableOperation, FableOperationFn} from "../types";

export const DEBUGGABLE = Symbol("DEBUGGABLE");

export function isOperationDebuggable<I, O>(enh: FableOperationFn<I, O>): enh is FableOperation<I, O> {
    return DEBUGGABLE in enh;
}

export function isDebuggable(anything: any): anything is Debuggable {
    return DEBUGGABLE in anything;
}

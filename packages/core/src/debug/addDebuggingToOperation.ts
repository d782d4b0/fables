import {DebugEnhancerData, FableOperation, FableOperationFn} from "../types";
import {BehaviorSubject, merge, Observable} from "rxjs";
import {map, tap} from "rxjs/operators";
import {getDebuggerName} from "./getDebuggerName";
import {DEBUGGABLE, isOperationDebuggable} from "./isOperationDebuggable";

export function addDebuggingToOperation<I, O>(
    op: FableOperationFn<I, O>,
    name_?: string
): FableOperation<I, O> {
    if (isOperationDebuggable(op)) {
        if (name_) {
            return Object.assign(op, {[DEBUGGABLE]: name_});
        }
        return op
    }

    const debugInput$: BehaviorSubject<I | undefined> = new BehaviorSubject<I | undefined>(undefined);
    const debugOutput$: BehaviorSubject<O | undefined> = new BehaviorSubject<O | undefined>(undefined);

    const newOperation: FableOperationFn<I, O> = input$ => {
        return input$.pipe(
            tap({
                next: input => debugInput$.next(input),
                complete: () => debugInput$.unsubscribe()
            }),
            op,
            tap({
                next: output => debugOutput$.next(output),
                complete: () => debugOutput$.unsubscribe()
            }),
        );
    }

    const name = name_ ? name_ : getDebuggerName(op);
    const debuggableNewOperation: FableOperation<I, O> = Object.assign(newOperation, {
        [DEBUGGABLE]: name,
        debugSnapshot(): DebugEnhancerData[] {
            return [
                {
                    name: this[DEBUGGABLE],
                    index: 0,
                    input: debugInput$.getValue(),
                    output: debugOutput$.getValue()
                }
            ]
        },
        debugObserve(): Observable<DebugEnhancerData> {
            return merge(
                debugInput$.pipe(
                    map(input => ({
                        name: this[DEBUGGABLE],
                        index: 0,
                        input
                    }))
                ),
                debugOutput$.pipe(
                    map(output => ({
                        name: this[DEBUGGABLE],
                        index: 0,
                        output
                    }))
                ),
            )
        },
        debugSubEnhancers(): string[] {
            return [this[DEBUGGABLE]];
        }
    });

    return debuggableNewOperation;
}

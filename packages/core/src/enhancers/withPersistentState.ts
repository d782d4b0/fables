import {FableEnhancer} from "../types";
import {defer, Observable} from "rxjs";
import {map, tap} from "rxjs/operators";
import {addDebuggingToOperation} from "../debug/addDebuggingToOperation";

export interface PersistentInput<I,O> {
    input: I,
    state?: O
}

export const withPersistentState = <I, O>(): FableEnhancer<I, O, PersistentInput<I, O>, O> => (
    (nextOp) => addDebuggingToOperation(
        (input$: Observable<I>) => defer(() => {
            let mostRecentState: O | undefined = undefined;
            const output$: Observable<O> = input$.pipe(
                map(input => {
                    const r: PersistentInput<I, O> = {
                        input,
                        state: mostRecentState
                    };
                    return r;
                }),
                nextOp,
                tap(output => {
                    mostRecentState = output;
                })
            );

            return output$;
        }),
        "persistentState"
    )
)

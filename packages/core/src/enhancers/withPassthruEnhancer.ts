import {FableEnhancer} from "../types";
import {addDebuggingToOperation} from "../debug/addDebuggingToOperation";

export function withPassthruEnhancer<I, O>(): FableEnhancer<I,O,I,O>;
export function withPassthruEnhancer<I, O, EI, EO>(): FableEnhancer<I,O,EI,EO>;

export function withPassthruEnhancer<I, O>(): FableEnhancer<I,O,I,O> {
    return (nextOp) => addDebuggingToOperation<I, O>(
        (input$) => input$.pipe(nextOp),
        "passthru"
    )
};

import {FableEnhancer} from "../types";
import {FableReducer} from "./withReducer";
import {addDebuggingToOperation} from "../debug/addDebuggingToOperation";

export const withMidstreamReducers = <I, O, NI, NO>(
    inputReducer: FableReducer<I,NI>,
    outputReducer: FableReducer<NO,O>
): FableEnhancer<I,O,NI,NO> => (
    (nextOp) => addDebuggingToOperation(input$ => (
        input$.pipe(
            inputReducer,
            nextOp,
            outputReducer,
        )
    ), "midstreamReducers")
)

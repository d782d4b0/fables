import {FableEnhancer} from "../types";
import {NEVER} from "rxjs";
import {addDebuggingToOperation} from "../debug/addDebuggingToOperation";

export const withTerminatingEnhancer = <I>(): FableEnhancer<I, never,never,never> => (
    () => addDebuggingToOperation(
        (input$) => NEVER,
        "terminating"
    )
);

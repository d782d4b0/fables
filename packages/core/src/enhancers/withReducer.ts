import {Observable} from 'rxjs';
import {FableEnhancer} from "../types";
import {addDebuggingToOperation} from "../debug/addDebuggingToOperation";

export type FableReducer<I, O> = (input$: Observable<I>) => Observable<O>;

export const withReducer = <I,O>(reducer: FableReducer<I,O>): FableEnhancer<I, O, never, never> => (
    () => addDebuggingToOperation((input$) => (
        input$.pipe(
            reducer,
        )
    ), "reducer")
);

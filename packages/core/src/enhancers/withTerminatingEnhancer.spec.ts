import {withTerminatingEnhancer} from "./withTerminatingEnhancer";
import {Subject} from "rxjs";
import {getDebuggerName} from "../debug/getDebuggerName";

describe('withTerminatingEnhancer', () => {
    it('should return a NEVER observable', () => {
        const output = jest.fn();
        const input$: Subject<number> = new Subject<number>();
        const obs = withTerminatingEnhancer()(null as any)(input$);
        obs.subscribe(output)
        expect(output).toBeCalledTimes(0);
    })
    it('should have the correct debugger name', () => {
        expect(getDebuggerName(withTerminatingEnhancer()(null as any))).toBe('terminating')
    })
})

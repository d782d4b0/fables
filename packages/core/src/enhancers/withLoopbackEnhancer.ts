import {FableEnhancer} from "../types";
import {Observable} from "rxjs";
import {addDebuggingToOperation} from "../debug/addDebuggingToOperation";

export const withLoopbackEnhancer = <I>(): FableEnhancer<I, I, never, never> => (
    () => addDebuggingToOperation(
        (input$: Observable<I>) => input$,
        "loopback"
    )
);

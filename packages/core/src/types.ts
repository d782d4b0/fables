import {Observable} from 'rxjs';
import {DEBUGGABLE} from "./debug/isOperationDebuggable";

// Fable and FableEnhancer are the MOST IMPORTANT types to understand
export type Fable<I,O> = FableFn<I,O> & Debuggable & {
    getState(): O | undefined;
};

export type FableEnhancer<I, O, NI, NO> = (nextOp: FableOperation<NI, NO>) => FableOperation<I, O>;
/*
export type FableEnhancerWithNext<I, O, NI, NO> = (nextOp: FableOperation<NI, NO>) => FableOperation<I, O>;
export type FableEnhancerWithoutNext<I, O> = () => FableOperation<I, O>;
export type FableEnhancer<I, O, NI, NO> = (
    ([NI] extends [never]
        ? [NO] extends [never]
            ? FableEnhancerWithoutNext<I, O>
            : FableEnhancerWithNext<I, O, NI, NO>
        : FableEnhancerWithNext<I, O, NI, NO>
    )
);
 */

export interface DebugEnhancerData {
    name: string;
    index: number;
    input?: any; // TODO: maybe type this generically in the future?
    output?: any; // TODO: maybe type this generically in the future?
}

// Instantiator you'll encounter for advanced use cases, but you can ignore it to get started
export type FableInstantiator<I, O> = (inputOperation: FableOperation<I, O>) => Fable<I, O>;

// FableFn and FableOperation are both transitory types and are only used internally
export type FableFn<I, O> = (action$?: Observable<I>) => Observable<O>;
export type FableOperation<I, O> = FableOperationFn<I, O> & Debuggable;
export type FableOperationFn<I, O> = (input$: Observable<I>) => Observable<O>;

export type Debuggable = {
    [DEBUGGABLE]: string, // contains the name of the enhancer
    debugSnapshot(): DebugEnhancerData[];
    debugObserve(): Observable<DebugEnhancerData>;
    debugSubEnhancers(): string[];
}

// insignificant change 2

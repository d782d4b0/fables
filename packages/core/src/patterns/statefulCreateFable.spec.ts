import {statefulCreateFable} from "./statefulCreateFable";
import {map, tap} from "rxjs/operators";
import {of} from "rxjs";

const TEST_VAL1 = "TEST_VAL1";
jest.useFakeTimers();

describe('statefulCreateFable', () => {
    it('should allow the user to define a valid observable using an arbitrary rxjs sequence', () => {
        const step1 = jest.fn();
        const step2 = jest.fn();
        const f$ = statefulCreateFable<number, number>(input$ => input$.pipe(
            tap(v => step1(v)), // tap(step1) breaks the types in webstorm for some reason
            map(v => (v.state || 0) + v.input)
        ));
        jest.runOnlyPendingTimers()
        f$().subscribe(step2);
        expect(step1).toBeCalledTimes(0);
        expect(step2).toBeCalledTimes(0);

        f$(of(1));
        expect(step1).toBeCalledTimes(1);
        expect(step1.mock.calls[0][0]).toStrictEqual({input: 1, state: undefined});
        expect(step2).toBeCalledWith(1);

        f$(of(1));
        expect(step1).toBeCalledTimes(2);
        expect(step1.mock.calls[1][0]).toStrictEqual({input: 1, state: 1});
        expect(step2).toBeCalledWith(2);
    })
})

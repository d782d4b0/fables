import {variableCreateFable} from "./variableCreateFable";
import {of} from "rxjs";

const STORED_VALUE1 = "STORED_VALUE1";
const STORED_VALUE2 = "STORED_VALUE2";
jest.useFakeTimers();

describe('variableCreateFable', () => {
    it('should store a value and surface it with getState', () => {
        const t$ = variableCreateFable<string>();
        jest.runOnlyPendingTimers()
        expect(t$.getState()).toBe(undefined);
        t$(of(STORED_VALUE1));
        expect(t$.getState()).toBe(STORED_VALUE1);
        t$(of(STORED_VALUE2));
        expect(t$.getState()).toBe(STORED_VALUE2);
    });

    it('should store a value and surface it to subscribe', () => {
        const fn = jest.fn();
        const t$ = variableCreateFable<string>();
        jest.runOnlyPendingTimers()
        t$().subscribe(fn);
        t$(of(STORED_VALUE1));
        expect(fn).toBeCalledWith(STORED_VALUE1);
        t$(of(STORED_VALUE2));
        expect(fn).toBeCalledWith(STORED_VALUE2);
        expect(fn).toBeCalledTimes(2);
    })
})

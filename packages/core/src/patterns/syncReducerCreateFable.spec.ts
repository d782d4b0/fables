import {SyncReducer, syncReducerCreateFable} from "./syncReducerCreateFable";
import {of} from "rxjs";

jest.useFakeTimers();

const counterReducer: SyncReducer<number,number> = (state = 0, action = 0) => {
    return state + action;
}

describe('syncReducerCreateFable', () => {
    it('should store a sync counter reducer', () => {
        const f$ = syncReducerCreateFable(counterReducer);
        jest.runOnlyPendingTimers()
        expect(f$.getState()).toBe(0); // initial state should be the default reducer return value
        f$(of(1)); // add 1
        expect(f$.getState()).toBe(1);
        f$(of(2)); // add 2
        expect(f$.getState()).toBe(3);
        f$(of(-1)); // subtract 1
        expect(f$.getState()).toBe(2);
    });
});

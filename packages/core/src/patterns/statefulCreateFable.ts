import {
    combineEnhancers,
    createFable,
    Fable,
    FableEnhancer,
    FableReducer,
    PersistentInput,
    withPassthruEnhancer,
    withPersistentState,
    withReducer
} from "..";

export function statefulCreateFable<I,O>(
    fable: FableReducer<PersistentInput<I,O>,O>,
): Fable<I, O>;

export function statefulCreateFable<I,O,EI,EO>(
    fable: FableReducer<PersistentInput<EI,EO>,EO>,
    enhancer: FableEnhancer<I, O, EI, EO>
): Fable<I, O>;

export function statefulCreateFable<I,O,EI,EO>(
    fable: FableReducer<PersistentInput<EI,EO>,EO>,
    enhancer: FableEnhancer<I, O, EI, EO> = withPassthruEnhancer<I, O, EI, EO>()
): Fable<I, O> {
    return createFable<I, O>(
        combineEnhancers(
            withReducer<PersistentInput<EI, EO>, EO>(fable),
            withPersistentState<EI, EO>(),
            enhancer
        )
    );
};

import {combineEnhancers, createFable, Fable, FableEnhancer, withLoopbackEnhancer, withPassthruEnhancer} from "..";

export function variableCreateFable<T>(): Fable<T, T>;

export function variableCreateFable<T,I,O>(
    enhancer_: FableEnhancer<I, O, T, T>
): Fable<I, O>;

export function variableCreateFable<T,I,O>(
    enhancer: FableEnhancer<I, O, T, T> = withPassthruEnhancer<I, O, T, T>()
): Fable<I, O> {
    return createFable<I, O>(
        combineEnhancers(
            withLoopbackEnhancer(), // no transform needed of the stored value, just shove it back into the output
            enhancer
        )
    );
};

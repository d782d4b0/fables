import {Fable, FableEnhancer, statefulCreateFable, withPassthruEnhancer} from "..";
import {map, startWith} from "rxjs/operators";

export type SyncReducer<S, A> = (state: S | undefined, action: A | undefined) => S;

export function syncReducerCreateFable<I, O>(
    reducer: SyncReducer<O, I>,
): Fable<I, O>;

export function syncReducerCreateFable<I,O,EI,EO>(
    reducer: SyncReducer<EO, EI>,
    enhancer_: FableEnhancer<I, O, EI, EO>
): Fable<I, O>;

export function syncReducerCreateFable<I,O,EI,EO>(
    reducer: SyncReducer<EO, EI>,
    enhancer: FableEnhancer<I, O, EI, EO> = withPassthruEnhancer<I, O, EI, EO>()
): Fable<I, O> {
    return statefulCreateFable<I, O, EI, EO>(
        input$ => (
            input$.pipe(
                map((input) => reducer(input.state, input.input)),
                startWith(reducer(undefined, undefined))
            )
        ),
        enhancer
    );
};

import {Fable, FableEnhancer, FableInstantiator} from "./types";
import {singletonInstantiator} from "./instantiators/singletonInstantiator";

export const createFable = <I,O>(
    enhancer: FableEnhancer<I, O, never, never>,
    instantiate: FableInstantiator<I, O> = singletonInstantiator<I, O>()
): Fable<I, O> => {
    // just in case an enhancer is written without TS... and the enhancer is malformed... we want to surface a good error
    const coreOp: never = (() => {
        throw new Error("The last FableEnhancer is attempting to call the nextEnhancer. Did you forget to put a Reducer last?");
    }) as any as never;

    return instantiate(enhancer(coreOp));
}

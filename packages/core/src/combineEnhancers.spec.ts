import {combineEnhancers} from "./combineEnhancers";
import {withReducer} from "./enhancers/withReducer";
import {withMidstreamReducers} from "./enhancers/withMidstreamReducers";
import {map} from "rxjs/operators";
import {of, Subject} from "rxjs";
import {withLoopbackEnhancer} from "./enhancers/withLoopbackEnhancer";
import {addDebuggingToOperation} from "./debug/addDebuggingToOperation";

const TEST_VAL1 = "TEST_VAL1";
const TEST_VAL2 = "TEST_VAL2";

describe('combineEnhancers', () => {
    it('should combine two enhancers into one, and execute their callbacks in the correct order', () => {
        const step1 = jest.fn(v => v);
        const step2 = jest.fn(v => v);
        const step3 = jest.fn(v => v);
        const step4 = jest.fn(v => v);
        const enhancer = combineEnhancers(
            withReducer<number, number>((input$) => input$.pipe(
                map(value => step2(value)+1))
            ),
            withMidstreamReducers<number, number, number, number>(
                (input$) => input$.pipe(
                    map(value => step1(value)+1),
                ),
                (input$) => input$.pipe(
                    map(value => step3(value)+1),
                ),
            )
        );
        const instance = enhancer((() => { throw new Error("this is never called"); }) as any as never);

        instance(of(0)).subscribe(step4);
        expect(step1).toBeCalledWith(0);
        expect(step2).toBeCalledWith(1);
        expect(step3).toBeCalledWith(2);
        expect(step4).toBeCalledWith(3);
    });

    // TODO: add variant for non-terminating combineEnhancers
    it('should return the first argument when only 1 is provided', () => {
        const arg = withLoopbackEnhancer();
        expect(combineEnhancers(arg)).toBe(arg);
    });

    it('should return a passthru enhancer when no args are provided', () => {
        const step1 = jest.fn(v => v);
        const step2 = jest.fn(v => v);

        const passthru = combineEnhancers<number, number>();
        expect(passthru).toBeDefined();

        const op = passthru(addDebuggingToOperation(input$ => input$.pipe(
            map(value => step1(value)+1)
        ), "debugOp"));

        op(of(0)).subscribe(step2);

        expect(step1).toBeCalledWith(0);
        expect(step2).toBeCalledWith(1);
    })

    it('should show a snapshot for all of the involved enhancers', () => {
        const result = jest.fn();
        const enhancer = combineEnhancers(
            withReducer<number, number>((input$) => input$.pipe(
                map(value => value+1))
            ),
            withMidstreamReducers<number, number, number, number>(
                (input$) => input$.pipe(
                    map(value => value+1),
                ),
                (input$) => input$.pipe(
                    map(value => value+1),
                ),
            )
        );

        const op = enhancer((() => { throw new Error("this is never called"); }) as any as never);

        // can't use a simple of() as the input because it'll cause the observable to complete and cleanup the debugger
        const input$ = new Subject<number>();
        op(input$).subscribe(result);
        const debugObserve = jest.fn();
        op.debugObserve().subscribe(debugObserve);

        input$.next(0);
        expect(result).toBeCalledWith(3);

        expect(op.debugSubEnhancers().length).toBe(2);
        expect(op.debugSnapshot()).toStrictEqual(
            [
                {"index": 0, "input": 1, "name": "reducer", "output": 2},
                {"index": 1, "input": 0, "name": "midstreamReducers", "output": 3},
            ]
        );

        expect(debugObserve.mock.calls.slice(0,4).map(v => v.output)).toEqual([undefined,undefined,undefined,undefined]);
        expect(debugObserve.mock.calls.slice(4).map(v => v[0])).toEqual([
            { index: 1, input: 0,  name: "midstreamReducers"},
            { index: 0, input: 1,  name: "reducer"},
            { index: 0, output: 2, name: "reducer"},
            { index: 1, output: 3, name: "midstreamReducers"},
        ]);
    });

    it('should flatten the snapshot for recursive combineEnhancer calls', () => {
        const result = jest.fn();
        const enhancer = combineEnhancers(
            combineEnhancers(
                withReducer<number, number>((input$) => input$.pipe(
                    map(value => value+1))
                ),
                withMidstreamReducers<number, number, number, number>(
                    (input$) => input$.pipe(
                        map(value => value+1),
                    ),
                    (input$) => input$.pipe(
                        map(value => value+1),
                    ),
                ),
                withMidstreamReducers<number, number, number, number>(
                    (input$) => input$.pipe(
                        map(value => value+1),
                    ),
                    (input$) => input$.pipe(
                        map(value => value+1),
                    ),
                )
            ),
            combineEnhancers(
                withMidstreamReducers<number, number, number, number>(
                    (input$) => input$.pipe(
                        map(value => value+1),
                    ),
                    (input$) => input$.pipe(
                        map(value => value+1),
                    ),
                ),
                withMidstreamReducers<number, number, number, number>(
                    (input$) => input$.pipe(
                        map(value => value+1),
                    ),
                    (input$) => input$.pipe(
                        map(value => value+1),
                    ),
                )
            )
        );
        const op = enhancer((() => { throw new Error("this is never called"); }) as any as never);

        // can't use a simple of() as the input because it'll cause the observable to complete and cleanup the debugger
        const input$ = new Subject<number>();
        op(input$).subscribe(result);
        input$.next(0);

        expect(op.debugSubEnhancers().length).toBe(5);
        const snapshot = op.debugSnapshot();
        expect(snapshot).toStrictEqual(
            [
                {"index": 0, "input": 4, "name": "reducer", "output": 5},
                {"index": 1, "input": 3, "name": "midstreamReducers", "output": 6},
                {"index": 2, "input": 2, "name": "midstreamReducers", "output": 7},
                {"index": 3, "input": 1, "name": "midstreamReducers", "output": 8},
                {"index": 4, "input": 0, "name": "midstreamReducers", "output": 9},
            ]
        );
    })

    it('should order properly with more than 3 enhancers', () => {
        const result = jest.fn();
        const enhancer = combineEnhancers(
            withReducer<number, number>((input$) => input$.pipe(
                map(value => value+1))
            ),
            withMidstreamReducers<number, number, number, number>(
                (input$) => input$.pipe(
                    map(value => value+1),
                ),
                (input$) => input$.pipe(
                    map(value => value+1),
                ),
            ),
            withMidstreamReducers<number, number, number, number>(
                (input$) => input$.pipe(
                    map(value => value+1),
                ),
                (input$) => input$.pipe(
                    map(value => value+1),
                ),
            ),
            withMidstreamReducers<number, number, number, number>(
                (input$) => input$.pipe(
                    map(value => value+1),
                ),
                (input$) => input$.pipe(
                    map(value => value+1),
                ),
            )
        );
        const op = enhancer((() => { throw new Error("this is never called"); }) as any as never);

        // can't use a simple of() as the input because it'll cause the observable to complete and cleanup the debugger
        const input$ = new Subject<number>();
        op(input$).subscribe(result);
        input$.next(0);

        expect(op.debugSubEnhancers().length).toBe(4);
        const snapshot = op.debugSnapshot();
        expect(snapshot).toStrictEqual(
            [
                {"index": 0, "input": 3, "name": "reducer", "output": 4},
                {"index": 1, "input": 2, "name": "midstreamReducers", "output": 5},
                {"index": 2, "input": 1, "name": "midstreamReducers", "output": 6},
                {"index": 3, "input": 0, "name": "midstreamReducers", "output": 7},
            ]
        );
    })
})

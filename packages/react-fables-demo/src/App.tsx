import React, {useState} from "react";
import {
    combineEnhancers,
    createFable,
    createSelector,
    singletonInstantiator,
    withPersistentState,
    withReducer
} from "@fables/core";
import {map} from "rxjs/operators";
import {useFable} from "@fables/react";
import {switchMap} from "rxjs/internal/operators/switchMap";
import {filter} from "rxjs/internal/operators/filter";
import {from} from "rxjs/internal/observable/from";
import {of} from "rxjs/internal/observable/of";
import {catchError} from "rxjs/internal/operators/catchError";
import {merge} from "rxjs/internal/observable/merge";
import {debounceTime} from "rxjs/internal/operators/debounceTime";

type LoginResponse = { success: boolean, error?: string };
function doFakeLogin(username: string, password: string) {
    return new Promise<LoginResponse>(resolve => {
        setTimeout(() => {
            const success = username === "bob" && password === "bob";
            resolve({
                success,
                error: !success ? "invalid username or password" : undefined,
            })
        }, 1000)
    })
}

interface UserData {
    username: string;
    heightCm: number;
}
function doFakeGetCurrentUserData() {
    return new Promise<UserData>(resolve => {
        setTimeout(() => resolve({ username: "bob", heightCm: 207 }), 2000)
    })
}

const startLoginRequest = (username: string, password: string) => ({ type: "START_LOGIN", username, password});
const startLogout = () => ({ type: "START_LOGOUT" });

type DoLoginAction = ReturnType<typeof startLoginRequest | typeof startLogout>;
type DoLoginState = null | LoginResponse | Error;

const loginState$ = createFable<DoLoginAction,DoLoginState>(
    combineEnhancers(
        withPersistentState(),
        withReducer(input$ => (
            merge(
                input$.pipe(
                    filter(input => input.action.type === "START_LOGOUT"),
                    map(v => null)
                ),
                input$.pipe(
                    filter(input => input.action.type === "START_LOGIN"),
                    debounceTime(250),
                    switchMap((input) => merge(
                        of(null),
                        doFakeLogin(input.action.username, input.action.password)
                    )),
                    catchError(error => of(error))
                )
            )
        )),
    ),
    singletonInstantiator<DoLoginAction, DoLoginState>(null)
);

const isUserLoggedIn$ = createSelector(loginState$, loginState => loginState !== null && !(loginState instanceof Error) && loginState.success)();

type UserDataState = null | UserData;
const userData$ = createFable<never, UserDataState>(
    combineEnhancers(
        withReducer(input$ =>
            merge(
                isUserLoggedIn$().pipe(
                    filter(isUserLoggedIn => isUserLoggedIn),
                    switchMap(_ => from(doFakeGetCurrentUserData()))
                ),
                isUserLoggedIn$().pipe(
                    filter(isUserLoggedIn => !isUserLoggedIn),
                    map(_ => null)
                )
            )
        )
    ),
    singletonInstantiator<never, UserDataState>(null)
);

export const App = () => {
  //const [state, dispatch] = useFable(myFable$);
  const [loginState, loginDispatch] = useFable(loginState$);
  const [isUserLoggedIn] = useFable(isUserLoggedIn$);
  const [userData] = useFable(userData$);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = ev => {
      ev.stopPropagation();
      ev.preventDefault();
      loginDispatch(startLoginRequest(username, password));
  }

  return (
      <div>
          <pre>
              Login state: {loginState instanceof Error ? 'error\n' : 'not error\n'}
              {JSON.stringify(loginState)}
          </pre>
          Is user logged in: {isUserLoggedIn ? "true": "false"}
          <pre>
              User data {JSON.stringify(userData)}
          </pre>

          {!isUserLoggedIn && (
              <form onSubmit={onSubmit}>
                  Username: <input name="username" onChange={ev => setUsername(ev.target.value)}/>
                  Password: <input name="password" onChange={ev => setPassword(ev.target.value)}/>
                  <input type="submit"/>
              </form>
          )}
          {isUserLoggedIn && (
              <button onClick={() => loginDispatch(startLogout())}>Logout</button>
          )}
      </div>
  );
}

import React from "react";
import {
    combineEnhancers,
    createFable,
    FableReducer,
    singletonInstantiator,
    Unit,
    withPersistentState,
    withReducer
} from "fables";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {useFable} from "@fables/react";
import {initFablesDevTools} from "@fables/redux-devtools";
import {switchMap} from "rxjs/internal/operators/switchMap";
import {filter} from "rxjs/internal/operators/filter";
import {from} from "rxjs/internal/observable/from";
import {of} from "rxjs/internal/observable/of";
import {catchError} from "rxjs/internal/operators/catchError";
import {merge} from "rxjs/internal/observable/merge";

function doFakeLogin(username, password) {
    return new Promise(resolve => {
        setTimeout(() => resolve(username == "bob" && password == "bob"), 1000)
    })
}

interface UserData {
    username: string;
    heightCm: number;
}
function doFakeGetUserData(username) {
    return new Promise(resolve => {
        setTimeout(() => resolve({ username: "bob", heightCm: 207 }), 2000)
    })
}

const startLoginRequest = (username, password) => ({ type: "START_LOGIN", username, password});

type DoLoginAction = ReturnType<typeof startLoginRequest>;
type DoLoginState = null | boolean | Error;

const doLogin$ = createFable<DoLoginAction,DoLoginState>(
    combineEnhancers(
        withPersistentState(),
        withReducer(input$ => (
            input$.pipe(
                filter(input => input.action.type === "START_LOGIN"),
                switchMap(input => merge(
                    of(null),
                    doFakeLogin(input.action.username, input.action.password)
                )),
                catchError(error => of(error))
            )
        )),
    ),
    singletonInstantiator<DoLoginAction, DoLoginState>(null)
);
umbreonFable$(of("RELOAD"));


export const App = () => {
    //const [state, dispatch] = useFable(myFable$);
    const [umbreonState, umbreonDispatch] = useFable(umbreonFable$);

    return (
        <div>
            UmbreonState: {JSON.stringify(umbreonState)}
            {umbreonState instanceof Error ? 'error' : 'not error'}
            {"value: "}
            {/*
          {state}
          <br/>
          <button onClick={() => dispatch({type: "INCREMENT"})}>Up</button>
          <button onClick={() => dispatch({type: "DECREMENT"})}>Down</button>
          */}
        </div>
    );
}

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.6](https://gitlab.com/D782D4B0/fables/compare/v1.2.5...v1.2.6) (2021-06-02)

**Note:** Version bump only for package @fables/react-fables-demo





## [1.2.5](https://gitlab.com/D782D4B0/fables/compare/v1.2.4...v1.2.5) (2021-05-31)

**Note:** Version bump only for package @fables/react-fables-demo





## [1.2.4](https://gitlab.com/D782D4B0/fables/compare/v1.2.3...v1.2.4) (2021-05-30)

**Note:** Version bump only for package @fables/react-fables-demo





## [1.2.3](https://gitlab.com/D782D4B0/fables/compare/v1.2.2...v1.2.3) (2021-05-30)

**Note:** Version bump only for package @fables/react-fables-demo





## [1.2.2](https://gitlab.com/D782D4B0/fables/compare/v1.2.1...v1.2.2) (2021-05-30)


### Bug Fixes

* eqeqeq error in react-fables-demo ([db3e6f0](https://gitlab.com/D782D4B0/fables/commit/db3e6f02b8e899c4b08cf5be2cbe91aff941ea82))
* missing build command in gitlab-ci, and broken build for the demo ([2c65da1](https://gitlab.com/D782D4B0/fables/commit/2c65da1b85df2bf86f4c7bac1288ea13c74e0481))





## [1.2.1](https://gitlab.com/D782D4B0/fables/compare/v1.2.0...v1.2.1) (2021-05-30)

**Note:** Version bump only for package @fables/react-fables-demo





# [1.2.0](https://gitlab.com/D782D4B0/fables/compare/v1.1.1...v1.2.0) (2021-05-30)

**Note:** Version bump only for package @fables/react-fables-demo





## [1.1.1](https://gitlab.com/D782D4B0/fables/compare/v1.1.0...v1.1.1) (2021-05-16)

**Note:** Version bump only for package @fables/react-fables-demo





# 1.1.0 (2021-05-16)


### Bug Fixes

* **react-fables-demo:** Broken imports from package renaming ([62dcafe](https://gitlab.com/D782D4B0/fables/commit/62dcafe38335e6c8e51a04e0f0b24482067b100c))


### Features

* initial test setup and MVP test coverage ([3033fa3](https://gitlab.com/D782D4B0/fables/commit/3033fa3ad81b2d2ce641e309b4882dd9b62ce9f7))



## 1.0.1 (2021-03-23)



# 1.0.0 (2021-03-23)

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.1.0 (2021-05-16)



## 1.0.1 (2021-03-23)



# 1.0.0 (2021-03-23)

**Note:** Version bump only for package @fables/common-deps

# Fables

An Async-native state management library built on RxJS. Native TS or JS, Modular, framework-agnostic or first-class React support.

A Fable is, logically, a singleton value store. It is represented as a function that takes an (optional) RxJS Observable of actions as input, and outputs a RxJS Observable of state as output.

[Quick comparison to Redux.](docs/compare_to_redux.md)

An example explains better than my ability to write:

```js
const DEFAULT_COUNTER_STATE = 0;

// statefulFableCreator is one of many helpful shortcut patterns... there's a raw `createFable` for power users
const myCounterFable$ = statefulFableCreator(persist$ =>
    // input$ is of type Observable<PersistentInput<ActionType, StateType>>
    // in this case, ActionType is just a string, StateType is just a number
    persist$.pipe(
        map(persist => {
            if(persist.input === "INCREMENT") {
                return persist.state + 1;
            } else if(persist.input === "DECREMENT") {
                return persist.state - 1;
            }
            return persist.state || DEFAULT_COUNTER_STATE;
        })
    )
);
// the most recently returned event is cached within the Fable and is available synchronously

// As soon as the creator is called, it is instantiated and live.
// Observe state (any framework):
const subscription = myCounterFable$().subscribe(state => console.log("The current counter value is", state));

// Dispatch actions (any framework):
myCounterFable$(of("INCREMENT"));
myCounterFable$(of("INCREMENT"));
myCounterFable$(of("INCREMENT"));
myCounterFable$(of("DECREMENT"));

// or dispatch multiple at once
myCounterFable$(of("INCREMENT","INCREMENT","INCREMENT"));

// cleanup/stop console logging -- if you use @fables/react, this is handled for you
subscription.unsubscribe();

// Observe state and dispatch actions (react component):
const MyComponent = (props) => {
    const [counter, dispatchCounter] = useFable(myCounterFable$); // automatically subscribes/unsubscribes
    
    return (
        <div>
            <p>Count: {counter}</p>
            {/* useCallback omitted for simplicity */}
            <button onClick={() => dispatchCounter("INCREMENT")}>Increment</button>
            <button onClick={() => dispatchCounter("DECREMENT")}>Decrement</button>
        </div>
    );
}

// Create a selector:
const DEFAULT_SELECTOR_STATE = 0;
const counterSquared$ = createSelector(myCounterFable$, counterState => counterState ** 2);
// Selectors are represented as Fables that do not accept any input actions. So,
// you can .subscribe() the same way, and you can use the useFable() React hook the same way with a selector, eg
// const [counterSquared, _uselessDispatchThisVarCanBeRemoved] = useFable(counterSquared$);

// Create a compound selector (from multiple Fables):
const someCompoundSelector$ = createSelector(
    combineFables(counterSquared$, someOtherFable$)(),
    ([counterSquaredState, someOtherFableState]) => counterSquaredState + someOtherFableState /* any valid selector */
)
// Compound selectors internally cache their results, and only update when either of their parents emit.
// Unlike redux, this means that state changes only go as far as necessary, instead of triggering *every* selector to
// recompute.
```

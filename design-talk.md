# Talk

State/Async Management in Modern UI Applications

Schedule:
1) Redux pros/cons
2) Tenets for a better framework
3) Fable introduction
4) Fable internals and common tasks
5) Tying Fables and the Tenets together

## The Land Before Now

Angular Services:
* State stored as variables within an object
* Difficult to test
* Difficult to understand state over time
* Manual notification of changes to state (or, ng-zone black magic)

React component state:
* Very simple
* Can be cleanly separated into reducers, but devs aren't forced
* Tedious to share and change state across components
* Often difficult to reuse common state patterns across components
* Async edge cases galore

Redux:
* Strong separation of display and state logic
* Selector pattern is very useful (centralization, refactors, etc)
* Large boilerplate effort (types, action creators, reducers, mapStateToProps, etc)
* Async support requires a middleware (thunk/saga/observable)
* Centralization into a single object gives unique strengths and weaknesses

## Why has Redux been so successful?

Alternatively, why hasn't something better replaced Redux?

```
  // A reducer:
  (state, action) => state

  // A selector:
  (state) => any

  // An action creator:
  (..any) => action
```
1) The core redux reducer pattern is very compelling. Reducers are easy to read, easy to test, easy to understand.
2) Selectors allow for compartmentalization of business logic.
3) Selectors are recursive without leaking internal selector implementation details to invokers (hereafter "Compound 
   Selectors"). This isolation means that selectors are simple to develop, extend, and reason about.
4) Selectors create a clean separation between the retrieval of data for the view component, and the storing of that data.
5) Action creators create a clean separation between the view component and the dispatching of notifications to the reducer


## What could redux do better?

### Async

eg, `fetch()` is not a first-class citizen in Redux.

Async has to be processed outside of redux and then pass events in. Monitoring redux from async processes is difficult
because you cannot easily monitor for state changes that your async process might actually care about. (Thunks, Sagas,
and redux-observable all solve this problem, but in very different ways, and with different limitations)

Sagas/Redux-observable are solid patterns but are still second-class citizens because you typically need glue
actions and corresponding reducer modifications. This is strongly against the Single-responsibility principle (SOLID)
because you cannot isolate the async logic and state to a single function, class, file, or (in many cases) even a single
directory.

Selectors play into the trouble: Selectors give a point in time value, and do not give ANY insight into how values
change over time.

### Compound selectors

Just to clarify what a compound selector is:
```
// simple selectors:
getSelectedBookIndex(state) => index
getAllBooks(state) => Book[]
// compound selector:
getSelectedBook(state) => getAllBooks(state)[getSelectedBookIndex(state)]
```

Compound selectors are useful, but they have weird limitations in redux:

* Cannot perform transformations of state without inefficient rerendering in target component
* reselect will allow for transformations of state, but this is kind of a hack
  * it's a hack because state exists in the memoization -- which lives outside of the state tree.
* reselect is a poor solution for selectors that are executed multiple times per render
  * Most people solve this by precomputing the result in the reducer, but this is potentially less efficient

### Type definitions

RootState, RootAction are difficult to define. Most applications use broad definitions which strictly weaken TS value

### One object tree means reducers MUST be combined into rootReducer

This isn't always straighforward. Eg, async import() of new reducers is hard`

## Tenets for a better state management framework:

1) Typescript is a first-class citizen (but should be usable in JS projects)
2) Elimination of the root reducer and root state
    1) But, retention of the flexibility that centralized control can offer
3) Preservation of the selector and action creators.
4) Unification of the async model (Saga/Epic/Thunk) and the reducer (S in SOLID)
5) A selector should be continuous. = Async Selectors

## Introducing Fables

```
// a Fable:
(Observable<action>?) => Observable<state>
```

A Fable is logically the same as a Redux Reducer, but it self-contains state as a singleton (BehaviorSubject), and its
input and output are Observables (streams over time of the underlying type).

That is a RxJS Observable. Explaining Observables is outside of the scope of this document. It is like a promise (push
model events), but it is continuous (many events over time), and provides functional transformations of the event stream
(map()/filter(), but also temporal transformations eg delay()/buffer()).

A counter example:

```ts
type CounterFableAction = "INCREMENT" | "DECREMENT"; // just for illustration, this example is JS

const counterFable$ = initCounterFable(0); // definition omitted for now // 0 = default
counterFable$().subscribe(state => console.log("Counter:", state));

counterFable$(of("INCREMENT","INCREMENT", "DECREMENT")).subscribe();
counterFable$(of("DECREMENT")).subscribe();
// output:
// Counter: 0 // <- this one happened beause on subscribe, fables instantly emit their current state. see [a] line
// Counter: 1 // <- these next four happened because of state transformations from [b] and [c] being "broadcast" to [a]
// Counter: 2
// Counter: 1
// Counter: 0
```

## Creating a Fable

```ts
const counterFable$ = createFable(
    combineEnhancers( // [a]
        withPersistentState(), // [e]
        withReducer(counterReducer), // [d] // will probably be defined inline in most projects, but separated here for clarity
    ),
    singletonInstantiator(0) // [c] // 0 = default value
);

const counterReducer = (input$) => { // [b]
    // input$ is an Observable<Unit<Action,State>>
    // return type is Observable<State>
    return input$.pipe(
        map(input => {
            if(input.action === "INCREMENT") {
                return input.state + 1;
            } else {
                return input.state - 1;
            }
        })
    )
}
```

Key takeaways:
1) the reducer itself is asynchronous. (in Redux, it is synchronous)
2) the Fable consists of many enhancers, which affect the reducer *somehow* (to be clarified later)
3) an instantiator is required to turn the "combined enhancers" into a hot Fable.

Note: these footnotes will probably make more sense if you come back later.

[a] Fables are composed using the enhancer pattern, which is inspired by Redux store enhancers and Express's `app.use()`.

[b] Fables internally use Observables of `Unit<Action,State>`, where Unit is effectively a input.

[c] A FableInstantiator turns a series of FableEnhancers into an actual Fable by instantiating the singleton.
[c] The instantiator just chops the State off the input (always passing in null), and chops the Action off the output.
[c] `singletonInstantiator` is shipped by default and fits most needs.

(not illustrated) A function that takes in `Observable<Unit<Action,State>>` and returns the same is called a
`FableOperation`. This is the Redux Reducer equivalent.

[d] withReducer creates an enhancer from a simpler reducer function.

[e] Fables do not automatically take the prior state from the reducer and feed it back in with the next action. The
`withPersistentState` enhancer adds this.

## What the fuck is a FableEnhancer/FableInstantiator anyway?

```
  Fable (createFable())
+------------------------------------------------------------------------------------------------------------------------+
|                                                                                                                        |
|             FableInstantiator                           FableEnhancer                             FableEnhancer        |
|   Actions +-------------------------+ State & Actions +-----------------------+ State & Actions +-------------------+  |
|   +------>+                         +---------------->+                       +---------------->+                   |  |
|           | SingletonInstantiator() |                 | withPersistentState() |                 |   withReducer()   |  |
|           |                         |                 |                       |                 | +---------------+ |  |
|     State |                         | State & Actions |                       | State & Actions | |               | |  |
|     <-----+                         +<----------------+                       +<----------------+ | Reducer       | |  |
|           +-------------------------+                 +-----------------------+                 | |               | |  |
|                                                                                        ^        | +---------------+ |  |
|                                                                                        +N       |                   |  |
|                                                                                Potentially many +-------------------+  |
|                                                                                FableEnhancers                          |
|                                                                                                                        |
+------------------------------------------------------------------------------------------------------------------------+
```

In this model, the State, Actions, and the "State & Actions" arrows all represent streams of their respective types,
not single instances.

The Instantiator actually creates the Fable instance, maintains the Fable instance internally, and provides the
communication primitives to the world outside the Fable.

The Instantiator is always "first" (when reading the above diagram left to right).

Most of the time, you will use the built in Instantiator unless your application has special needs regarding the Fable
lifecycle.

withReducer() is just a shortcut to create an Enhancer from a Reducer. You could also say that a Reducer is just a
simplified representation of an Enhancer. The rightmost reducer has a "next enhancer," just like the others, but it is
always a noop (and isn't meant to be called -- the last enhancer should do some meaningful work).

A Fable contains zero to many Enhancers. Enhancers are covered in the next section.

## Show me what an enhancer looks like

An enhancer is a way to assemble a rxjs observable:

```js
const enhancer = nextEnhancer => input$ => input$.pipe(
       
    tap(v => console.log(v)), // input$ is the actions/state coming into this enhancer  
    // transformations before the next enhancer go here
    
    // then, when the enhancer wishes to, pass the stream into the next enhancer like this
    getNextEnhancer,

    // this is the output arrow from the next enhancer pointing back into this one
    tap(v => console.log(v)),
    
    // return value of the enhancer gets returned to the parent enhancer in the same way the enhancer above returned
    // its value to us
);
```

An enhancer receives as its input two things, the stream of state and actions so far, and a function to invoke to get
the next enhancer (the next-next enhancer is injected into the next one for you, automatically). Since enhancers have
the full input stream, and have full control over the output, an enhancer can do anything that a reducer could do,
including extreme actions such as deleting events from the stream (so the inner Enhancers don't see it), or deleting
state changes from the output.

A practical use case would be when implementing something such as Redux Devtools's time travel feature. You would
want a time travel enhancer which, when the user selects an old state, overrides the "true state" of the reducer.

## How do I write a Fable Selector?

In Redux, Selectors are functions that take in the state tree, and return some type T. We can define Redux selectors
like this: `type ReduxSelector<T> = (state: RootState) => T`

With Fables, a Selector is just another Fable, except it doesn't accept actions.
`type FableSelector<T> = Fable<never, T>` (not a real type, just for illustration)

You should alter your thinking at this point:
* Anything you can do within a Fable, you can do with a Selector, and vice-versa. Async etc
* Anywhere that accepts a Fable will also accept a Selector.

You can write a selector by hand, but you probably shouldn't do this:

```ts
// builds on the above-defined counterFable$
// a redux-like selector:
const counterSquaredSelector = (counterState) => counterState ** 2;

const counterSquaredFable$ = createFable(
    combineEnhancers(
        withReducer((input$) => {
            // COMPLETELY IGNORE input$ -- we don't CARE about the inputs
            return counterFable$().pipe( // fables are *still* Observables
                map(counterState => counterSquaredSelector(counterState))
            );
        }),
    ),
    singletonInstantiator(0)
);

counterSquaredFable$().subscribe((v) => console.log(v)); // outputs the square of counterSquaredFable$
```

Unless your selector has to do something weird, there is a shortcut to create a FableSelector from a Redux-like
selector. This is logically identical to the prior example:

```ts
const counterSquaredSelector = (counterState) => counterState ** 2;

const counterSquaredFable$ = createSelector(
    withPassthruEnhancer(), // does nothing
    singletonInstantiator(0), // the instantiator
    counterSquaredFable$, // the Fable that we should execute the selector against
    counterSquaredSelector // the selector
);

counterSquaredFable$.subscribe((v) => console.log(v)); // outputs the square of counterSquaredFable$
```

## How do I write a compound Fable Selector?

Let's assume we have two defined Fable Selectors, `getSelectedBookIndex$` and `getAllBooks$`, and we wish to define a
new Fable Selector, `getSelectedBook$`:

```ts
const combinedFables$ = combineFables(getSelectedBookIndex$, getAllBooks$);
// combinedFables$ is a Fable of the shape: Fable<never, [getSelectedBookIndex$.state, getAllBooks$.state]> // (pseudocode)
// Essentially, a Fable with the state of a input of the state of the input types, in the order they were provided.
// This is a logical mirror of reselect's combineSelectors()

const getSelectedBook$ = createSelector(
    withIdentityEnhancer(),
    singletonInstantiator(),
    combinedFables$, // separated for clarity, you probably wouldn't separate in practice unless you had a good reason
    ([selectedBookIndex, allBooks]) => allBooks[selectedBook] // redux-like selector
);
```

## How do I read the value of a Selector from a FableReducer?

Let's say you have a Fable named `isUserLoggedIn$` (definition not shown) which will emit true if the user is logged in.

Let's say you wish to write a Fable named `userLogoutMonitor$`, which detects when the user's login state changes in
order to wipe the user's browser cache. You could write a standalone Fable, and dispatch actions to it all throughout the
application, but you already have logic tracking the user's login state (as evidenced by the existence of `isUserLoggedIn$`).

In this case, it is better to derive a Fable from `isUserLoggedIn$`. It would look like this:

```ts
const userLogoutMonitor$ = createFable(
    combineEnhancers(
        withPersistentState(),
        withReducer((input$) => {
            // input$ is ignored -- this is self-triggering/`never` action type
            isUserLoggedIn$(/* see: special footnote */).pipe(
                switchMap(isUserLoggedIn => {
                    if(!isUserLoggedIn) {
                        // whatever cache busting logic here
                        return of("SUCCESS");
                    } else {
                        return of();
                    }
                })
            );
        }),
    ),
    singletonInstantiator(0)
);
```

special footnote: this is the location that you would pass in actions to a Fable, if you wished to, but Selectors should
never do this as a selector is (by definition) a pure function on the output -- but you may have a non-selector use case
for passing in actions, which could be appropriate, hence this footnote's existence.

### Why wouldn't you just simplify this by using a "naked" .subscribe() on isUserLoggedIn$?

```ts
isUserLoggedIn$().subscribe(isUserLoggedIn => {
    if(!isUserLoggedIn) {
        // whatever cache busting logic here
    }
})
```

Well, maybe that's fine. In the case where you wish to monitor the output of an asynchronous/background process like
this (and potentially generate alerts if it fails), representing it as yet another Fable makes it simpler because it is
uniform.

You can imagine an aggregateErrors$ Fable, which subscribes to a large number of Fables and aggregates their error states
for presentation in the UI. With a naked subscribe, this becomes harder because you lose the tooling of Fables.

### What about centralized control?

You may have observed that there is no "central" Fable "store" parallel in Redux. Each Fable is its own store.

So, this means that the RootReducer/RootState problems have disappeared. But also that we don't have a `store` to 
apply enhancers uniformly to all fables.

You can solve this simply by defining a creator function within your application, of course tuning to your application's
specific needs:

```ts
const fableCreator = (Fable: FableReducer<A,S>) => (
    createFable(
        combineEnhancers(
            withPersistentState(),
            withReducer(Fable),
        ),
        singletonInstantiator(undefined)
    )
);
// you SHOULD also define a fableSelectorCreator function similar to the above

const myFable$ = fableCreator(reducer);
```

## Tenet Recap

### 1) Typescript is a first-class citizen (but should be usable in JS projects)

The Fable primitives are written in TS. All input and output types are defined.

No weird type reflection in the library, so it's fully usable by a plain JS project.

### 2) Elimination of the root reducer and root state
`1) But, retention of the flexibility that centralized control can offer`

The root reducer and root state are both eliminated due to the independent nature of Fables.

Centralized control and flexibility is possible through functional programming (creators).

### 3) Preservation of the selector and action creators.

Selectors were illustrated above. Action creators are unmodified from redux and are compatible with Fables as-is:

```
const createAction = () => ({ type: "MY_ACTION", payload: { whatever: true } });
someFable$(of(createAction())).subscribe();
```

### 4) Unification of the async model (Saga/Epic/Thunk) and the reducer (S in SOLID)

Self-evident. This is a Fable, by definition.

### 5) A selector should be continuous. = Async Selectors

Fable Selectors are just Fables, so they retain the asychronous/continuous properties.

## Links

https://gitlab.com/C2D03041/fables

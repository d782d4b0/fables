# Redux Comparison

#### Fable Strengths (in common with Redux):

* Rich enhancer-based "plugin" system that allows for central control of Fables/Store
  * Not to be confused with "A centralized store" -- just centralized development controls
* Strong separation of display logic (React et al) and state logic
  * Action Creators bind your display logic to your store
  * Selectors bind your store to your display logic
* Encourages the Action Creator pattern
  * Allows for action serialization (websockets), store auditibility (ala redux devtools)
* Selectors and Compound Selectors allow for rich application logic to be stored in a standard format
  * `allBooks$` and `selectedBookId$` fables can be easily combined into a `selectedBook$` fable
* Similar to the prior, Compartmentalization of business logic is simplified with Selectors.
  * Selectors easily build on each other, so the smallest unit of business logic is representable


#### Fable Strengths (distinct from Redux):

Alternative framing: `Redux Weaknesses (not exhibited by Fables)`

* Reduced boilerplate effort
  * Types, Action Creators, Reducers, mapStateToProps, etc
  * Redux has largely not aligned on standard patterns about how to use it, which is good for flexibility but bad
    for cutting down this boilerplate.
    * Fables addresses this by allowing devs to cut boilerplate where desired, and by shipping standard pattern shortcuts.
* Built-in asynchronous model means standardization across projects:
  * No Thunks/Sagas/Observables random choice between teams
  * Thunks have problems with store auditability, and are not good at monitoring the store
  * Sagas are a bit magic-incantation-y for my preferences. So, the only good remaining choice was Observables
* No Centralized "fable store" exists, which means:
  * no centralized `store.subscribe()` callbacks, the leading source of Redux slow-down
    * Fable subscriptions are only updated when their underlying fable(s) update.
  * no computing selector updates on **every** state change
    * Selectors internally subscribe to their parent Fables, so, only when their parents update does a selector update
  * in TS, no RootState type definition (notoriously a pain)
  * not necessary to use immutable state transformations to detect changes (although still encouraged if using React, since React benefits from it)

# Talk

State/Async Management in Modern UI Applications

## Schedule

1) Prerequisites
2) Fables Introduction
3) Fables React Integration
4) Fables Tenets
5) Comparisons (Redux, React useState())

section timestamps in the description

## Prerequisites

1) RxJS minimal working knowledge
    1) Watch my crash course: TODO link
2) Fundamental React knowledge (useState, Context)
    1) and ideally some knowledge about Redux also

## Tenets for a better state management framework:

1) Typescript is a first-class citizen (but should be usable in JS projects)
2) No centralization at runtime (eg, no root reducer/root state)
    1) But, retention of the flexibility that centralized control can offer
3) Preservation of the selector and action creators.
4) Unification of the async model (Saga/Epic/Thunk) and the reducer (S in SOLID)
5) A selector should be continuous. = Async Selectors

## Introducing Fables

```
// a Fable:
(Observable<action>?) => Observable<state>
```

A Fable self-contains state as a singleton, and its input and output are Observables. (this is logically the same as
an "async Redux Reducer")

A Fable `Counter` example:

```ts
type CounterFableAction = "INCREMENT" | "DECREMENT"; // just for illustration, this example is JS

const counterFable$ = initCounterFable(0); // definition omitted for now // 0 = default
counterFable$().subscribe(state => console.log("Counter:", state));

counterFable$(of("INCREMENT","INCREMENT", "DECREMENT"));
counterFable$(of("DECREMENT"));
// output:
// Counter: 0 // <- this one happened beause on subscribe, fables instantly emit their current state. see [a] line
// Counter: 1 // <- these next four happened because of state transformations from [b] and [c] being "broadcast" to [a]
// Counter: 2
// Counter: 1
// Counter: 0

counterFable$(of("INCREMENT")).subscribe(/*...*/) // can be combined if you wish
```

## Creating a Fable

```ts
const counterFable$ = createFable(
    combineEnhancers(
        withPersistentState(),
        withReducer(counterReducer), // will probably be defined inline in most projects, but separated here for clarity
    ),
    singletonInstantiator(0) // 0 = default value
);

const counterReducer = (input$) => {
    // input$ is an Observable<Unit<Action,State>>
    // return type is Observable<State>
    return input$.pipe(
        map(input => {
            if(input.action === "INCREMENT") {
                return input.state + 1;
            } else {
                return input.state - 1;
            }
        })
    )
}
```

A fable has multiple Enhancers. In practice, you will probably not write your own enhancers, instead consume them from
libraries that do things you want to use and apply to your Fables.

When Actions are passed into a Fable, the actions flow down through the Enhancers in order. The last Enhancer is called
"the reducer" (by convention -- it is still an Enhancer). The reducer converts the action into some state, which flows
back up through the enhancers, bottom to top.

Each Enhancer can call any RxJS operation on the input or the output, if it wishes. 

```ts
combineEnhancers(
    withEnhancer1(),
    withEnhancer2(),
    withEnhancer3(),
    withReducer()
)

// Sample execution order:
// myFable$("SOMEACTION")
// Enhancer1 -- receives SOMEA CTION
// Enhancer2 -- receives SOMEACTION
// Enhancer3 -- receives SOMEACTION
// Reducer -- converts SOMEACTION into NEWSTATE
// Enhancer3 -- receives NEWSTATE
// Enhancer2 -- receives NEWSTATE
// Enhancer1 -- receives NEWSTATE
// myFable$().subscribe() -- receives NEWSTATE
```

## HTTP (fetch()) with a Fable

```ts
const umbreonFable$ = createFable<any,any>(
    combineEnhancers(
        withPersistentState(),
        withReducer(input$ => (
            input$.pipe(
                filter(input => input.action === "RELOAD"),
                switchMap(input => merge(
                    of(null),
                    from(fetch("https://pokeapi.co/api/v2/pokemon/umbreona").then(r => r.json()))
                )),
                catchError(error => of(error))
            )
        )),
    ),
    singletonInstantiator(null)
);
umbreonFable$(of("RELOAD"));

umbreonFable$().subscribe(/*...*/)
// possible .subscribe() values:
// * null (default/loading)
// * object (loaded)
// * error (network/parsing error)
```

What I didn't write:
1) A separate Reducer and async logic
2) Actions/types/action creators specifically as "reducer glue"

## How do I write a Fable Selector?

A Selector (broadly) is a function that derives some information from one or multiple "larger" parent objects. We say
that the selector "selects" values that it cares about from its parent state(s).

So, we can say that "isUmbreonFableLoading" is derived from or selects from "umbreonFable", so it is appropriate to
model as a selector.

With Fables, a Selector is just another Fable, except it doesn't accept actions.
`type FableSelector<T> = Fable<never, T>` (for illustration, not an official type)

Key Takeaways:
* This is different from Redux etc which uses synchronous selectors
* Anything you can do within a Fable, you can do with a Selector, and vice-versa. ( = internally the same)
* Anywhere that accepts a Fable will also accept a Selector, and vice-versa. ( = externally the same)

You can write a selector by hand, but you probably shouldn't do this:

```ts
// builds on the above-defined umbreonFable$
// a synchronous or "redux-like" selector:
const isUmbreonFableLoading = (umbreonState) => umbreonState == null;

const isUmbreonFableLoading$ = createFable(
    combineEnhancers(
        withReducer((input$) => {
            // COMPLETELY IGNORE input$ -- we don't CARE about the inputs
            return umbreonFable$().pipe( // fables are *still* Observables
                map(umbreonState => isUmbreonFableLoading(umbreonState))
            );
        }),
    ),
    singletonInstantiator(0)
);

isUmbreonFableLoading$().subscribe((v) => console.log(v)); // outputs the square of counterSquaredFable$
```

Unless your selector has to do something weird, there is a shortcut to create a FableSelector from a synchronous
selector. This is logically identical to the prior example:

```ts
const isUmbreonFableLoading = (umbreonState) => umbreonState == null;

const isUmbreonFableLoading$ = createSelector(
    umbreonFable$, // the Fable that we should execute the selector against
    isUmbreonFableLoading // the selector
)(/* optional enhancer and instantiator */);

isUmbreonFableLoading$.subscribe((v) => console.log(v)); // outputs a stream of true or false
```

### Building a Login system

### Compound Fable Selectors

### Monitoring Selectors from within other Fables

### Why wouldn't you just simplify this by using a "naked" .subscribe() on isUserLoggedIn$?

```ts
isUserLoggedIn$().subscribe(isUserLoggedIn => {
    if(!isUserLoggedIn) {
        // whatever cache busting logic here
    }
})
```

Well, maybe that's fine. In the case where you wish to monitor the output of an asynchronous/background process like
this (and potentially generate alerts if it fails), representing it as yet another Fable makes it simpler because it is
uniform.

You can imagine an aggregateErrors$ Fable, which subscribes to a large number of Fables and aggregates their error states
for presentation in the UI. With a naked subscribe, this becomes harder because you lose the tooling of Fables.

### What about centralized control?

You may have observed that there is no "central" Fable "store" parallel in Redux. Each Fable is its own store.

So, this means that the RootReducer/RootState problems have disappeared. But also that we don't have a `store` to 
apply enhancers uniformly to all fables.

You can solve this simply by defining a creator function within your application, of course tuning to your application's
specific needs:

```ts
const fableCreator = (Fable: FableReducer<A,S>) => (
    createFable(
        combineEnhancers(
            withPersistentState(),
            withReducer(Fable),
        ),
        singletonInstantiator(undefined)
    )
);
// you SHOULD also define a fableSelectorCreator function similar to the above

const myFable$ = fableCreator(reducer);
```

## Tenet Recap

### 1) Typescript is a first-class citizen (but should be usable in JS projects)

The Fable primitives are written in TS. All input and output types are defined.

No weird type reflection in the library, so it's fully usable by a plain JS project.

### 2) Elimination of the root reducer and root state
`1) But, retention of the flexibility that centralized control can offer`

The root reducer and root state are both eliminated due to the independent nature of Fables.

Centralized control and flexibility is possible through functional programming (creators).

### 3) Preservation of the selector and action creators.

Selectors were illustrated above. Action creators are unmodified from redux and are compatible with Fables as-is:

```
const createAction = () => ({ type: "MY_ACTION", payload: { whatever: true } });
someFable$(of(createAction())).subscribe();
```

### 4) Unification of the async model (Saga/Epic/Thunk) and the reducer (S in SOLID)

Self-evident. This is a Fable, by definition.

### 5) A selector should be continuous. = Async Selectors

Fable Selectors are just Fables, so they retain the asychronous/continuous properties.

## Links

https://gitlab.com/C2D03041/fables
